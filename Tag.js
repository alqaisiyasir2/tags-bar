class Tag{
    constructor(options){
        try {
            this.parentHtmlTag = document.getElementById(options.parentHtmlTag);
            this.actionOnNewTags = options.action
            this.trigerAction  = options.trigerAction
            this.tagsArray =[]
            this.tagsSuggestionEnabled = 0;

            this.firstSpanTag = this.createFirstEmptySpan()
            this.createInputForm()
            this.insertNodeAfter(this.inputFeild, this.firstSpanTag)
            this.lastSpanTag = this.createLastEmptySpan()

            this.parentHtmlTag.addEventListener("focus" ,()=>{
                this.butBorderOnTaglistDiv()
            }, true);

            this.parentHtmlTag.addEventListener("blur" , ()=>{
                this.removeBorderFromTaglistDiv()
            },true);

            if(typeof options.tagsSuggestionEnabled != "undefined" ){
                if(options.tagsSuggestionEnabled == 1 || options.tagsSuggestionEnabled == 0 ){
                    this.tagsSuggestionEnabled = options.tagsSuggestionEnabled 
                    this.displaySuggestions = options.displaySuggestions
                }   
            }
            if(typeof options.dataSource != "undefined" && options.dataSource != null){
                this.dataSource = options.dataSource
                this.createTagsFromDataSource(options.dataSource())
                //this.actionOnNewTags(this.tagsArray)
            }
        }catch(error) {
            
        }

    }
    createFirstEmptySpan(){
            let span = document.createElement('span')
            span.setAttribute("id", "firstSpan");  
            this.parentHtmlTag.appendChild(span)
            return span
    }
    createLastEmptySpan(){
        let span = document.createElement('span')
        span.setAttribute("id", "lastSpan");  
        this.parentHtmlTag.appendChild(span)
        return span
        }
    butBorderOnTaglistDiv(){
        this.parentHtmlTag.style.border = "1px solid #66BFFF";
        this.parentHtmlTag.style.boxShadow = "0px 0px 0px 4px  #D9EFFF";
            //createEmptyInput(document.getElementById("taglist"))
    }
    removeBorderFromTaglistDiv(){
        this.parentHtmlTag.style.border = "";
        this.parentHtmlTag.style.boxShadow = "0px 0px 0px 0px  #D9EFFF";
            //createNewTagOnBlur()
            //document.getElementById("taginput").remove()
    }
        
    //this function will be called every time we type inside the input feild
    /*
        this function also will check if we click on the backspace key
        if we keep pressing the backspace and at the same time there are no chars left 
        this function takes the value of the  tag before the input field, take its value and 
        assign it to the input field.
    */
    onKeydownIfBackSpacePressedAndNoValueInInputThenEditTagBefor(event){
        //checking if the user pressed on backspace and input feild is empty 
        if(event.keyCode == 8 &&  this.inputFeild.value === ""){
            //if the last element before the input feild is not empty then
            //we take its value and put it inside the text feild 
            if( this.inputFeild.previousSibling.innerText !== ""){
                //getting the value of the element before the input feild
                let valueOfTheTagBeforeTheInputFeild  =  this.inputFeild.previousSibling.innerText+" ";
                let nodeBeforeInputFeild = this.inputFeild.previousSibling
                //removing the tag that come before the input feild
                this.setTagAttributeTo(this.inputFeild,
                [
                    {name:"data-id", value:nodeBeforeInputFeild.getAttribute("data-id")},
                    {name:"index", value:nodeBeforeInputFeild.getAttribute("index")}
                ])
                this.deleteTagFromDom(nodeBeforeInputFeild)
                this.deleteTagFromTagsArray(nodeBeforeInputFeild)
                //putting the value of the deleted tag inside the input feild 
                this.inputFeild.value = valueOfTheTagBeforeTheInputFeild;
            }
        }
    }
    getNodePostion(node){
            let i = 0;
            while( (node = node.previousSibling) != null ) 
            i++;
            return i;
        }
    deleteTagFromTagsArray(node){
        try {
            if(node == null || typeof node == "undefined"){
                throw ("Error in deleteTagFromTagsArray(node): Node can not be undefined or null")
            }
            this.tagsArray[node.getAttribute("index")] = null

        } catch (error) {
            console.log(error)
        }
    }
    /*
    This function will create new tag when we click on the space bar
    inside the input feild
    */
    createNewTagOnBlur(){
                if(this.inputFeild.value .length > 1 && this.inputFeild !== ""  ){
                    this.createNewTagAndInsertItBeforeInput(
                        this.inputFeild.value,
                        this.inputFeild.getAttribute("data-id"),
                        this.inputFeild.getAttribute("index")
                    );
                }
                this.setTagAttributeTo(this.inputFeild,[
                        {name:"data-id", value:null},
                        {name:"index", value:null},

                ])
                this.emptyInputValue(this.inputFeild)

                this.insertNodeBefore(this.inputFeild, this.lastSpanTag)
                this.inputFeild.focus()
    }
    onTypeIfSpacebarPressedCreateTagBeforeInputAndResetInputValueAndAttributes(event){
            // console.log("onTypeIfSpacebarPressedCreateTagBeforeInputAndResetInputValueAndAttributes" , "creating new tag before input and empty input value")
            if(event.keyCode == 32 || event.keyCode == 13){
                    if(this.inputFeild.value.length > 1 && this.inputFeild.value !== ""  ){
                        let tagId = this.inputFeild.getAttribute("data-id")
                        this.createNewTagAndInsertItBeforeInput(this.inputFeild.value, tagId); 

                    }
                    this.setTagAttributeTo(this.inputFeild,[
                        {name:"data-id", value:null},
                        {name:"index", value:null}
                    ])
                    this.emptyInputValue(this.inputFeild)


                   

            }
    }
    createTagFromSugestion(){
                let tagInfo = this.dataset
                $("#taginput").val(tagInfo.tagname)
                this.createNewTagAndInsertItBeforeInput(tagInfo.tagname, tagInfo.tagid)
                $("#taginput").val('')
                $("#suggestion").css("display","none")
    }
    createTag(inputTagText, id = null){
        try {
            inputTagText = this.deleteSpacesFromString(inputTagText)
            let newTag  = document.createElement("span")
            let tagText = document.createTextNode(inputTagText);
            let cIcon   = document.createElement("a");
            cIcon.setAttribute("class", "s-tag--dismiss ");    
            let cIconText = document.createElement("i");
            cIconText.setAttribute("class","cTimes fa fa-times fa-1");
            cIcon.appendChild(cIconText);
            cIcon.appendChild(cIconText);
            newTag.appendChild(tagText);
            newTag.appendChild(cIcon);
            newTag.setAttribute("class", "s-tag rendered-element activeTags")
            newTag.addEventListener("click" ,(event)=>{
                this.clickedTag = event.target
                this.tagAction()
            }); 
            newTag.setAttribute("data-id", id) 
            return newTag

        } catch (error) {
            console.log(error)
        }

    }
    createNewTagAndInsertItBeforeInput(inputTagText, id = null){
        try{
            if(id !== null){
                id = parseInt(id)
            }
            inputTagText = this.deleteSpacesFromString(inputTagText)
            let newTag = this.createTag(inputTagText, id)
            this.parentHtmlTag.insertBefore(newTag, this.inputFeild);
            let newTagPosition = this.getNodePostion(newTag)
            newTag.setAttribute("index", newTagPosition) 
            this.tagsArray[newTagPosition] = {id:id, tagText:inputTagText, tagPositionInDom:newTagPosition}
            this.actionOnNewTags(this.deleteNullsFromArray(this.tagsArray))
        }catch(error){
            console.log(error)
        }

    }
    deleteNullsFromArray(array){
       return array.filter((value, index)=>{
            if(value == null){
                return 0
            }
            return 1
        })
    }
    createNewTagAndInsertItBeforeLastSpan(inputTagText, id=null){
        let newTag = this.createTag(inputTagText, id=null)
        this.parentHtmlTag.insertBefore(newTag, document.getElementById("lastSpan"));
    }  
    setTagAttributeTo(htmlTag, attributes){
        attributes.forEach(attribute => {
            htmlTag.setAttribute(attribute.name, attribute.value)
        });
    }
    insertNodeBefore(node, referenceNode){
        referenceNode.parentNode.insertBefore(node, referenceNode);

    }
    insertNodeAfter(node, referenceNode){
        referenceNode.parentNode.insertBefore(node, referenceNode.nextSibling);
    }
    emptyInputValue(input){
        input.value = ""
    }
    createInputForm(value="", elementId = null, index=null){
           let input = document.createElement("input")
               input.setAttribute("type", "text") 
               input.setAttribute("id", "taginput")
               input.setAttribute("index", index)
               input.setAttribute("data-id", elementId)
               input.setAttribute("style", "width:60px")
               input.setAttribute("style", "border:.5px red solid; width:60px")
               input.value = value 
               input.addEventListener("keypress", (event)=>{
                    this.onTypeIfSpacebarPressedCreateTagBeforeInputAndResetInputValueAndAttributes(event)
               })

               //when we click inside html tag
               this.parentHtmlTag.addEventListener("click", (event)=>{ 
                    this.createNewTagOnBlur(event)
               }, true)


                input.addEventListener("keypress",(event)=>{
                    if(this.trigerAction == 1){
                        if(event.keyCode == 32){
                            //this.actionOnNewTags(this.tagsArray)
                        }
                    }else{
                        //this.actionOnNewTags(this.tagsArray)
                    }
                    if(this.tagsSuggestionEnabled == 1){
                        this.displaySuggestions(
                                this.dataSource(
                                        this.inputFeild.value
                                )
                        )
                    }
                })
                input.addEventListener("keydown", (event)=>{
                    this.onKeydownIfBackSpacePressedAndNoValueInInputThenEditTagBefor(event)
                })
                this.inputFeild   = input
    } 
    createTagFromInputIfInputHasValue(input){
        /*
        checking if the input field has value if yes we take this value create new tag from it
        and insert it before the input field 

        we do this when we click on other tag while we are editing tag so we don't lose the information 
        of the tag that we work on. 

        after the creation of the tag we delete input feild 
        */
        if(this.inputFeild.value.length > 1 && this.inputFeild.value !== ""){
            //getting input value
            let inputvalue = this.inputFeild.value;
            //creating new tag from input value
            this.createNewTagAndInsertItBeforeInput(
                inputvalue,
                this.inputFeild.getAttribute("data-id")
            );
        }
    }
    createTagInPostion(newNodePostion, inputTagText ){
                        let tagList = document.getElementById("taglist")
                        let newTag  = document.createElement("span")
                        let tagText = document.createTextNode(inputTagText);
                        let cIcon   = document.createElement("a");
                                      cIcon.setAttribute("class", "s-tag--dismiss ");
                        let cIconText = document.createElement("i");
                        cIconText.setAttribute("class","cTimes fa fa-times fa-1");
                        cIcon.appendChild(cIconText);
                        cIcon.appendChild(cIconText);
        
                        newTag.appendChild(tagText);
                        newTag.appendChild(cIcon);
        
                        newTag.setAttribute("class" ,"s-tag rendered-element") 
                        newTag.setAttribute("data-id", null) 
                        newTag.addEventListener("click" ,tagAction); 
                        this.inputFeild.parentNode.insertBefore(newTag, inputFeild.parentNode.children[newNodePostion]);
        }     
    createInputFromTag(tag){
        //getting clicked tag object
        //console.log("clickedTag", this.clickedTag)

        //getting the text from the tag 
        let clickedTagValue = tag.innerText;
        //getting the position of the clicked tag in tag list
        let clickedTagPosition = this.getNodePostion(tag)
        //remove input field 
        //here we remove the clicked tag 
        //we create new input field in the same position as the clicked tag and assigne the value of the clicked tag 
        //fo it 

        //this.deleteTagFromTagsArray(this.clickedTag.getAttribute("index"))
        this.createInputForm(
            clickedTagValue,
            tag.getAttribute("data-id"),
            tag.getAttribute("index")                   
        );
        this.insertNodeBefore(this.inputFeild, tag)

    }
    tagAction(){
            // console.log(this.clickedTag)
            if(this.clickedTag.tagName == "SPAN")
                {
                    this.createTagFromInputIfInputHasValue(this.inputFeild)
                    this.inputFeild.remove()
                    this.createInputFromTag(this.clickedTag)
                    this.inputFeild.focus()
                    this.deleteTagFromTagsArray(this.clickedTag)
                    this.deleteTagFromDom(this.clickedTag)
                }
            else if(this.clickedTag.tagName == "I"){
                this.deleteTag()
            }
           
    }
    deleteTagFromDom(tag){

        tag.remove()
    }
    deleteTag(){
            let clickedTagDeleteIcon = event.target
            let tagToBedeleted = clickedTagDeleteIcon.parentNode.parentNode;
            let getTagToBedeletedPostion = this.getNodePostion(tagToBedeleted);
            
            if( this.inputFeild.value.length > 1 &&  this.inputFeild.value !== ""){
                let inputFeildValue = this.inputFeild.value
                this.createNewTagAndInsertItBeforeInput(inputFeildValue);
                this.inputFeild.remove();
            }else
                this.inputFeild.remove();
                this.deleteTagFromTagsArray(tagToBedeleted.getAttribute("index"))
                tagToBedeleted.remove(this.parentHtmlTag.childNodes[getTagToBedeletedPostion]);
            let afterElement = this.parentHtmlTag.childNodes[getTagToBedeletedPostion]
            this.createInputForm();
            this.insertNodeBefore(this.inputFeild, afterElement)

           
    }
    deleteSpacesFromString(string){
        return string.replace(/\s/g, '')

    }
    getTagIndex(tag){
        return parseInt(tag.getAttribute("index"))
    }
    getTagId(tag){
        return tag.getAttribute("data-id")
    }
    getHtmlTagPreviousSibling(htmlTag){
        return htmlTag.previousSibling
    }
    isThereDuplicateNameInTagList(tagsList){
        let tagNamesArray = []
        $.each(tagsList, function(i, tag){
            tagNamesArray.push(tag.nameTag)
        })
        return (new Set(tagNamesArray).size !== tagsList.length )
    }
    getDuplicateNamesFromTagsList(tagsList){
        let duplicateNames = []
        for(let i = 0; i< tagsList.length; i++){
            for(let j = i+1; j < tagsList.length; j++ ){
                if(tagsList[i].nameTag == tagsList[j].nameTag){
                    duplicateNames.push(tagsList[i])
                }
                      
            }
        }
        return duplicateNames
        }
    thereIsSameTagTextInTagsArray(tagText){
        tagText = this.deleteSpacesFromString(tagText)
        let foundTag = this.tagsArray.filter((tag, index)=>{
            if(tag.tagText == tagText)
                return tagText;
            else
                return 0
        })        
        return foundTag.length

    }
    checkIfTagsCountWithinTheLimits(tagsList){
         return tagsList.length > 5
    }
    createTagsFromDataSource(tagsData){
        console.log(tagsData)
        $.each(tagsData,(index,tag)=>{
            this.createNewTagAndInsertItBeforeInput(tag.tagText,  tag.id)
        })
    }

}